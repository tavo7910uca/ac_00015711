org 	100h

;GUSTAVO ERNESTO MOLINA AYALA 00015711
section .text

	call 	text ;listo
	call 	pointer ;listo
	call 	frase_1 ;listo
	call 	frase_2 ;listo
	call 	frase_3 ;listo
	call	waitk  ;listo

	int 	20h

text:	
    mov 	ah, 00h
	mov	    al, 02h
	int 	10h
	ret

pointer: 
    mov	    ah, 01h
	mov 	ch, 59H
	mov 	cl, 3CH;   IRGB
	int 	10h
	ret

w_char:	mov 	ah, 09h
	mov 	al, cl
	mov 	bh, 0h
	mov 	bl, 00001111b
	mov 	cx, 1h
	int 	10h
	ret

waitk: 
    mov 	ax, 0000h
	int 	16h
	ret

cursor:
    mov 	ah, 02h
	mov 	dx, di 	;columna
	mov 	dh, 9d 	;fila
	mov 	bh, 0h
	int 	10h
	ret

cursor2:
    mov 	ah, 02h
	mov 	dx, di  
	mov 	dh, 15d 
	mov 	bh, 0h
	int 	10h
	ret

cursor3:
    mov 	ah, 02h
	mov 	dx, di  
	mov 	dh, 20d 
	mov 	bh, 0h
	int 	10h
	ret

frase_1:	
    mov 	di, 6d
escribo:	
    mov 	cl, [msg+di-6d]
	call    cursor
	call 	w_char
	inc 	di
	cmp 	di, len
	jb		escribo
	ret

frase_2:	
    mov 	di, 12d
escribo2:	
    mov 	cl, [msg2+di-12d]
	call    cursor2
	call 	w_char
	inc	    di
	cmp 	di, len2
	jb		escribo2
	ret

frase_3:	
        mov 	di, 1d
escribo3:	
    mov 	cl, [msg3+di-1d]
	call    cursor3
	call 	w_char
	inc	    di
	cmp 	di, len3
	jb		escribo3
	ret

section .data
msg		db 	"Lo' maliantes quieren krippy, krippy, krippy"
len 	equ	$-msg+6

msg2	db 	"To'a las babys quieren kush, kush, kush, kush, kush"
len2	equ	$-msg2+12

msg3	db 	"Lo' gansters quieren krippy, krippy, krippy, krippy, krippy"
len3 	equ	$-msg3+1