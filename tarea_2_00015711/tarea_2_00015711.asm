;Gustavo Molina 00015711
;Ejercicio 1. Tomando como base los últimos 5 dígitos de su carnet, cada digito representa una de las notas de las evaluaciones 
;que serán la nota del primer parcial de una materia ficticia, por medio de un promedio, calcule la nota de su primer parcial ficticio 
;y escriba en la fila de celdas de memoria de 200h a 20Fh el comentario correspondiente:
;
;10 = “Perfecto solo Dios”                                         5 = “En el segundo”
;
;9 = “Siempre me esfuerzo”                                         4 = “Me recupero”
;
;8 = “Colocho”                                                     3 = “Hay salud”
;
;7 = “Muy bien”                                                    2 = “Aun se pasa”
;
;6 = “Peor es nada”                                                1 = “Solo necesito el 0”

org 100h;

mov al, 1d
mov ah, 5d
mov bl, 7d
mov bh, 1d
mov cl, 1d

mov [210h], al
mov [211h], ah
mov [212h], bl
mov [213h], bh
mov [214h], cl

;cache
mov	ax, 0d
mov	bx, 0d
mov	cx, 0d
mov	dx, 0d

mov	ax, 5d	
mov	bx, 0d	
mov	cx, 0d		
mov dx, 0d	;bandera

suma:	mov cx, [210h + bx] 
	    add dl,cl		
	    inc bx		;+1
	    cmp	ax,bx
	    ja 	suma

	    mov	[27Ah], dl ;suma notas
	    mov	ax, 0000h
	    mov	al, dl
	    DIV bl

mov	[27Bh],al ;respuesta 

;mensaje para promedio 15/5=3
mov word [201h], "HA"
mov word [203h], "Y"
mov word [205h], "SA"
mov word [208h], "LU"
mov word [20Ah], "D"

;=======================================================================

;Ejercicio 2. El presidente Bayib Nukele, de un país ficticio, necesita presentar datos de una proyección de casos de la pandemia COVID-19, 
;para lo cual su equipo de expertos ha encontrado la maravillosa y precisa formula precisa: “comenzando con dos casos el día uno, cada tres 
;días se duplica el número”. Usted es el encargado de las gráficas, tiene que dejar una buena impresión. De manera dinámica, coloque 11 
;estimaciones partiendo del día 3, de tres en tres es decir primera celda día 3, segunda día 6, y así; una estimación por cada una o dos
;celdas de memoria, desde la celda 210h llenando toda la fila, cuando pase de 255 necesitará usar dos celdas de memoria para guardar el número.

org 100h;
mov     ax, 4h	;empieza en 2 el dia 1 y el dia 3 el doble 4
mov		bx, 0d
;menores que 100
menor:	mov [210h + bx], ax	
		mov cx,2h	
		mul	cx	
		inc	bx
		cmp	ax, 100h 	
		jb	menor  
;mayores 
mayor:	mov [210h + bx], ax
	    mov cx,2h
    	mul	cx
	    inc	bx 
	    inc	bx
	    cmp	ax, 4097d
	    jb	mayor

;=======================================================================

;Ejercicio 3. Escriba de manera dinámica los primeros 16 números de la sucesión de Fibonacci, cada uno en una o dos celda de memoria, 
;desde la celda 220h llenando toda la fila, cuando pase de 255 necesitará usar dos celdas de memoria para guardar el número. 

mov	ax, 0000h	
mov	bx, 0000h	
mov	cx, 0000h	;fn2
mov	dx, 0000h	;fn1

mov	bl, 2d	;2
mov dl, 1d 	;1
mov	[220h], cl	;f0
mov	[221h], dl	;f1

fibonacci:	mov	ax, dx	
	        add	ax, cx	
	        mov	[220h+bx], ax 
	        mov	cx, dx	
        	mov	dx, ax 	
	        inc bl 	
        	cmp	bl, 14d	
	        jb	fibonacci	;regresamos

;si pasa lo realizamos de nuevo 
mov	ax, dx	
add	ax, cx	
mov	[220h+bx], ah
inc bl 	
mov	[220h+bx], al
mov	cx, dx	
mov	dx, ax 

int 20h