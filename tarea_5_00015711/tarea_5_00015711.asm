;Gustavo Molina
;Tarea 5
;----------------------------------------------
org 	100h

section .text

	call 	graphicsFunction
	xor 	di, di   
;------------------------------------------------

iteration1:	
	mov 	cx, 100d 
	add		cx, di	 
	mov		dx, 10d 
	call 	showpixel
	inc 	di
	cmp 	di, 120d
	jne 	iteration1
	xor		di, di

iteration2:	
	mov 	cx, 100d
	mov		dx, 10d
	add		dx, di
	call 	showpixel
	inc 	di
	cmp 	di, 160d
	jne 	iteration2
	xor 	di, di   

iteration3:	
	mov 	cx, 100d 
	add		cx, di	 
	mov		dx, 170d 
	call 	showpixel
	inc 	di
	cmp 	di, 121d
	jne 	iteration3
	xor 	di, di   

iteration4:
	mov 	cx, 220d 	 
	mov		dx, 140d
	add		dx, di 
	call 	showpixel
	inc 	di
	cmp 	di, 30d
	jne 	iteration4
	xor 	di, di   

iteration5:	
	mov 	cx, 130d 
	add		cx, di	 
	mov		dx, 140d 
	call 	showpixel
	inc 	di
	cmp 	di, 90d
	jne 	iteration5
	xor 	di, di

iteration6:	
	mov 	cx, 130d 	 
	mov		dx, 105d
	add		dx, di 
	call 	showpixel
	inc 	di
	cmp 	di, 35d
	jne 	iteration6
	xor 	di, di   

iteration7:	
	mov 	cx, 130d 
	add		cx, di	 
	mov		dx, 105d 
	call 	showpixel
	inc 	di
	cmp 	di, 71d
	jne 	iteration7
	xor 	di, di

iteration8:
	mov 	cx, 200d 	 
	mov		dx, 75d
	add		dx, di 
	call 	showpixel
	inc 	di
	cmp 	di, 30d
	jne 	iteration8
	xor 	di, di   

iteration9:	
	mov 	cx, 130d 
	add		cx, di	 
	mov		dx, 75d 
	call 	showpixel
	inc 	di
	cmp 	di, 70d
	jne 	iteration9
	xor 	di, di

iteration10:	
	mov 	cx, 130d 	 
	mov		dx, 40d
	add		dx, di 
	call 	showpixel
	inc 	di
	cmp 	di, 35d
	jne 	iteration10
	xor 	di, di   

iteration11:	
	mov 	cx, 130d 
	add		cx, di	 
	mov		dx, 40d 
	call 	showpixel
	inc 	di
	cmp 	di, 91d
	jne 	iteration11
	xor 	di, di

iteration12:	
	mov 	cx, 220d 
	mov		dx, 10d
	add		dx, di 
	call 	showpixel
	inc 	di
	cmp 	di, 30d
	jne 	iteration12
	call 	keyboard
	int 	20h

graphicsFunction:
	mov		ah, 00h
	mov		al, 13h
	int 	10h
	ret

showpixel:	
	mov		ah, 0Ch
	mov		al, 1111b
	int 	10h
	ret
;--------------------------------------

keyboard: 	
	mov		ah, 00h
	int 	16h
	ret

section .data